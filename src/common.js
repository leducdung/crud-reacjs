
export const category = {
  food : 'Food',
  desert: 'Desert',
  drink: 'Drink',
}

export const listProduct = [
  {
    id: 1,
    name: 'Dapibus ac facilisis in',
    category: category.food,
    price: 100
  },
  {
    id: 2,
    name: 'Morbi leo risus',
    category: category.desert,
    price: 9.5
  },
  {
    id: 3,
    name: 'Porta ac consectettur ac',
    category: category.drink,
    price: 10
  },
  {
    id: 4,
    name: 'Vestibulum ar eros',
    category: category.food,
    price: 120
  }
]
