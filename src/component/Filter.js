import React, { Component } from "react";
import {  } from "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
// import './App.css';
export default function Filter(props) {
  const { products } = props
  return (
    <div className="container border p-4">
      <p> Filter</p>
      <div className="row">
        <div className="col-md-3">
          <label htmlFor="inputAddress2" className="form-label">
            Name
          </label>
          <input
            type="text"
            className="form-control"
            id="inputAddress2"
            placeholder="Item's name..."
          />
        </div>
        <div className="col-md-3">
          <label htmlFor="inputState" className="form-label">
            Category
          </label>
          <select id="inputState" className="form-select">
            <option selected>All</option>
            <option>...</option>
          </select>
        </div>
        <div className="col-md-2">
          <label htmlFor="inputCity" className="form-label">
            Min Price
          </label>
          <input type="text" className="form-control" id="inputCity" />
        </div>
        <div className="col-md-2">
          <label htmlFor="inputZip" className="form-label">
            Max Price
          </label>
          <input type="text" className="form-control" id="inputZip" />
        </div>
      </div>
    </div>
  );
}
