import React, { Component } from "react";
import {} from "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
// import './App.css';
export default function Menu(props) {
  const { products, setOrders, orders } = props;
  const addOrder = (id) => {

    const getOrder = products.find(product => product.id === id)
    console.log(getOrder);
    if(getOrder) {
      setOrders([...orders, getOrder])
    }

    console.log(orders);
  }
  return (
    <table class="table border">
      <thead>
        <tr>
          <th scope="col">NO</th>
          <th scope="col">Name</th>
          <th scope="col">Category</th>
          <th scope="col">Pice</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        {products.map((product) => {
          return (
            <tr>
              <th scope="row">{product.id}</th>
              <td>{product.name}</td>
              <td>{product.category}</td>
              <td>$ {product.price}</td>
              <td>
                {" "}
                <button className="btn btn-primary" style={{ float: "left" }} onClick={() => addOrder(product.id)} >
                  <strong>+</strong>Add to order
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
