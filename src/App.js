import "./App.css";
import {} from "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Filter from "./component/Filter";
import Menu from "./component/Menu";
import MyOrder from "./component/MyOrder";
import React, { useState } from 'react';
import { listProduct } from "./common";

function App() {
  const [products, setProducts] = useState(listProduct);
  const [orders, setOrders] = useState([]);

  return (
    <div className="App">
      <header className="container">
        {/* table */}
        <div class="alert alert-primary" role="alert">
          FA Restaurant
        </div>
        <Filter products = {products} ></Filter>

        <h1>1.Menu</h1>
        <Menu products = {products} setOrders = {setOrders} orders = {orders} ></Menu>

        <h1>1.My order</h1>
        <MyOrder setOrders = {setOrders} orders = {orders} ></MyOrder>
      </header>
    </div>
  );
}

export default App;
